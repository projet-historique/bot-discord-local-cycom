# Work with Python 3.6
import discord
from datetime import datetime
import asyncio
import threading
import time
from discord.ext import commands

TOKEN = 'bot token'

client = commands.Bot(command_prefix='!')
status = False
quarantine = False

def under_closed():
    while True:
        now = datetime.now()
        hour = now.strftime("%H")
        global status
        if (int(hour) >= 22 or int(hour) <= 8) and status:
            status = False
            asyncio.run(client.change_presence(activity=discord.Game('Local fermé !')))
            print("Ferme à 22h")
        time.sleep(300)

@client.event
async def on_ready():
    close_local = threading.Thread(target=under_closed, args=())

    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    print('The bot is ready !')
    if status:
        str = 'ouvert !'
    else:
        str = 'fermé !'
    await client.change_presence(activity=discord.Game('Local {}'.format(str)))
    close_local.start()

@client.command()
@commands.check_any(commands.has_role('Bureau Cycom'), commands.has_role('Faiseur de robot'))
async def open(ctx: commands.context):
    global status
    global quarantine

    if status:
        await ctx.send('Local is already open !')
        try:
            await ctx.send(ctx.guild.get_role(666920704708706304).mention + ' Move your f*cking ass!')
        except AttributeError:
            await ctx.send('Obviously no one\'s coming and I cannot ping them')
    else:
        status = True
        await client.change_presence(activity=discord.Game('Local ouvert !'))
        if quarantine:
            quarantine = False
            try:
                await ctx.send(ctx.guild.get_role(666920704708706304).mention + ' Local is not under quarantine anymore')
            except AttributeError:
                await ctx.send('Local is not under quarantine anymore')
        else:
            try:
                await ctx.send(ctx.guild.get_role(666920704708706304).mention + ' Local is now open !')
            except AttributeError:
                await ctx.send('Local is now open !')

@client.command()
@commands.check_any(commands.has_role('Bureau Cycom'), commands.has_role('Faiseur de robot'))
async def close(ctx: commands.context):
    global status
    global quarantine

    if not status:
        if quarantine:
            await ctx.send('Local is not under quarantine anymore but it\s still closed')
        else:
            await ctx.send('Local is already closed !')
    else:
        status = False
        await ctx.send('Local is now closed !')
        await client.change_presence(activity=discord.Game('Local fermé !'))

@client.command()
@commands.check_any(commands.has_role('Bureau Cycom'), commands.has_role('Faiseur de robot'))
async def corona(ctx: commands.context):
    global status
    global quarantine

    if not quarantine:
        status = False
        quarantine = True
        await ctx.send('Local is now under quarantine')
        await client.change_presence(activity=discord.Game('Local condamné jusqu\'à nouvel ordre'))
    else:
        await ctx.send('Local is already under quarantine')

@client.command()
@commands.check_any(commands.has_role('Bureau Cycom'), commands.has_role('Faiseur de robot'))
async def shutdown(ctx: commands.context):
    await Client.commandes.logout()

client.run(TOKEN)
