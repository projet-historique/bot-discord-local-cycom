FROM python:3.7-alpine

RUN mkdir /usr/src/bot/
COPY localbot.py /usr/src/bot/
COPY entrypoint.sh /
WORKDIR /usr/src/bot
RUN chmod +x /entrypoint.sh

COPY requirements.txt /requirements.txt
RUN chmod +x /requirements.txt

RUN apk update --no-cache && apk upgrade --no-cache
RUN apk add --no-cache python3 libressl-dev gcc musl-dev
RUN LIBRARY_PATH="/lib:/usr/lib"
RUN python3 -m pip install -r /requirements.txt --no-cache-dir

CMD ["/entrypoint.sh"]
